## Database opslag

### Innotec

* Admin					
* Adviser					
* Supervisor

### Firmaer

* EDoc
* EShop

### Dokumenter
* Adgangen til dokumenter er anonym og ukontrolleret.
* For at få adgang til dokumenter sender rådgiveren et link til firmaet.

### Oprettelse af dokumenter
Rådgiveren opretter dokumenter i et simpelt flow
1. Vælg kunde
2. Vælg arbejdssted
3. Vælg produktvariant
4. Vælg dokument type
5. Oplys
   * Oprettet af
   * Godkendt af
   * Dato
   * Beskrivelse (valgfrit) men kan lette identificering

#### Data opsamling for dokumenter
På baggrund af den valgte dokument type skal følgende data angives

##### For APB type
* Opbevaring af
   * Produkt
   * PVM (Masker)
   * PVM (Handsker)
   * PVM (Briller/Skærm)
 * Placering af førstehjælpsudstyr
 * Placering af øjenskylleflaske
 * Deponering af spild/affald

##### For KAPV type
   * Produkt
   * Egenskaber
   * Eksponering
   * Forebyggelse
   * Referencer
   * Alternativer
   * Forbedringer
   * Opfølgningsdato eller tidsrum

## Rådgiver opgaver
For at rådgiveren kan oprette dokumenter er det rådgiverens opgave at oprette et eller flere arbejdssteder med relation til firmaet som bruger produkterne.

Dokumenter kan kun oprettes med reference til et arbejdsted for et firma. Det betyder at uanset om firmaet er lille eller stort - skal der være et tilknyttet arbejdssted.

Innotec's kunde rådgivere har givet udtryk for at de ønsker at kontrollere om en virksomhed - via appen - kan genbestille produkter som er i brug på virksomheden.

Samtidig har det været et ønske fra Innotec at rådgivere skal kunne give en eller flere kontakt personer adgang til at oprette dokumenter i eget regi.

## Administrative opgaver
Administrative opgaver kan tildeles en hvilken som helst Innotec medarbejder.

De administrative opgaver er
* Oprettelse og ajourføring af produkter
* Oprettelse og ajourføring af produkt varianter
* Oprettelse og ajourføring af PVM
* Oprettelse og ajourføring af PVM varianter

## Afrunding
Denne beskrivelse af flow er omvendt logisk og medfører derfor at udvikling og kode følger denne omvendte logik.

For at sige det kort

* Slutbruger kan ikke komme til før rådgiveren
* Rådgiveren kan ikke komme til før Administrator 

Det er som at bygge et hus - du starter ikke med at hænge gardiner op og male vægge.


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTUxNTcyNzAwNF19
-->
